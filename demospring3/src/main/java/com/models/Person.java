package com.models;


import javax.validation.constraints.Size;

public class Person {

    private int id;

    @Size(min = 1,message = "Should not be empty!")
    @Size(max = 20,message = "So long name! Rewrite it under 20 character!")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public Person(){

    }

}

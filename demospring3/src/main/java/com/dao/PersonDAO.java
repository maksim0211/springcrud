package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import com.models.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class PersonDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;
    public PersonDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static int PEOPLE_COUNT = 0;
    private static String URL = "jdbc:postgresql://localhost:5432/first_db";
    private static String LOG = "postgres";
    private static String PASS = "12345";


    public List<Person> index() {
        return jdbcTemplate.query("SELECT * FROM Person",new PersonMapper());
    }

    public Person show(int id) {
        return (Person) jdbcTemplate.query("SELECT * FROM Person WHERE id=?", new Object[]{id},new PersonMapper()).stream().findAny().orElse(null);
    }

    public void save(Person person) {
        jdbcTemplate.update("INSERT INTO Person (name) VALUES(?)",person.getName());
    }

    public void update(int id, Person person) {
        jdbcTemplate.update("UPDATE Person SET name=? WHERE id=?",person.getName(),id);
    }

    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM Person WHERE id=?",id);
    }
}
